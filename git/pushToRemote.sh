#!/usr/bin/env bash

#    Copyright 2018-2018 PeopleWare n.v.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

if [[ $# -ne 1  ]]; then
    echo "ERROR: requires a target git URI as argument"
    exit 1
fi

echo Adding target remote to local repository …
git remote add target $1

echo Adding target remote done. Pushing to target remote …
git push target `git rev-parse --abbrev-ref HEAD` --tags

echo Push to target remote done. Removing target remote …
git remote remove target

echo Done.
