# git

- [`tagTravisGithub.sh`](tagTravisGithub.md): tag a successful Travis run on Github
- [`tagBitbucket.sh`](tagBitbucket.md): tag a successful Pipelines run on Bitbucket
- [`pushToRemote.sh`](pushToRemote.md): push the current head to a target remote
