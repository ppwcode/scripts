# Tag a successful Bitbucket Pipelines run on Bitbucket

## Prerequisites

This script requires

- to be run on [Bitbucket Pipelines] (or at least have environment variables `BITBUCKET_BUILD_NUMBER`)
- `git` in the `PATH`

This script depends on
[Bitbucket push-back functionality](https://community.atlassian.com/t5/Bitbucket-Pipelines-articles/Pushing-back-to-your-repository/ba-p/958407).

## What it does

The script tags the repository with `bitbucket/${BITBUCKET_BUILD_NUMBER}`, and pushes the tag to the Bitbucket
repository.

The `BITBUCKET_BUILD_NUMBER` is padded to 5 digits.

## Usage

Do not use this script in pipelines that run on tags that match `bitbucket/${BITBUCKET_BUILD_NUMBER}`, to prevent
getting in an infinite loop.

[bitbucket pipelines]: https://confluence.atlassian.com/bitbucket/build-test-and-deploy-with-pipelines-792496469.html
