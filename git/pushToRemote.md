# Push the current HEAD to the given target git remote

## Prerequisites

This script requires

- `git` in the `PATH`

## What it does

The script

- adds the given remote to the current repository
- pushes the current branch and all tags to the given remote

## Usage

    > ./scripts/git/pushToRemote <target_git_uri>

E.g., to push to the repository `ppwcode/scripts` at Github:

    > ./scripts/git/pushToRemote https://<CREDENTIALS>@github.com/ppwcode/scripts.git

We suggest putting the `<CREDENTIALS>` in a secret environment variable when you use this script in a CI system. For
most target git systems, you can use an application specific password, or a specific key instead of your regular user
name and password as `<CREDENTIALS>`. Otherwise, use `USERNAME:PASSWORD` for basic authentication.
