# Azure login with Service Principal

Login to Azure as the Service Principal whose credentials have been provided.

In order for this to work correctly, the script **must be called with a "dot space script" command**, otherwise
the script will be called under another shell and be unable to update the environment variables for the current shell.

i.e. it should be called like this:
```bash
. ./azLoginWithServicePrincipal.sh
```
**NOT**
```
./azLoginWithServicePrincipal.sh
```

You can pass additional arguments for the login too:
```bash
. ./azLoginWithServicePrincipal.sh --allow-no-subscriptions
```

## Prerequisites

This script requires
- environment variables for authentication to Azure to be set. Specifically, the script requires:
  - `$ARM_TENANT_ID`
  - `$ARM_CLIENT_ID`
  - `$ARM_CLIENT_SECRET`

