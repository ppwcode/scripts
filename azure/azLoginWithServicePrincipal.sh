#!/usr/bin/env bash

#    Copyright 2018-2018 PeopleWare n.v.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# This script will login to azure as a service principal with the provided environment credentials.
#
# To work, the script expects the following environment variables to be present and valid:
#  * ARM_CLIENT_ID,
#  * ARM_CLIENT_SECRET,
#  * ARM_TENANT_ID

if [ -z $ARM_TENANT_ID ]; then
    >&2 echo "Environment variable ARM_TENANT_ID not set. Cannot authenticate to Azure."
    exit 1
elif [ -z $ARM_CLIENT_ID ]; then
    >&2 echo "Environment variable ARM_CLIENT_ID not set. Cannot authenticate to Azure."
    exit 1
elif [ -z $ARM_CLIENT_SECRET ]; then
    >&2 echo "Environment variable ARM_CLIENT_SECRET not set. Cannot authenticate to Azure."
    exit 1
else
    az login --service-principal --username "${ARM_CLIENT_ID}" --password "${ARM_CLIENT_SECRET}" --tenant "${ARM_TENANT_ID}" "${@}"
fi
