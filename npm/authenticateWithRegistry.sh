#!/usr/bin/env bash

#    Copyright 2018-2018 PeopleWare n.v.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Create an `~/.npmrc` file, to get access to the npm registry defined in a project specific `.npmrc`.
#
# To work, the environment variable `NPM_REGISTRY_TOKEN` must be set up, and the registry must be specified in the
# project specific `.npmrc`.

if [ -f ~/.npmrc ]; then
    echo "There already is an ~/.npmrc file. Not changing npm registry authentication."
elif [ -z $NPM_REGISTRY_TOKEN ]; then
    >&2 echo "Environment variable NPM_REGISTRY_TOKEN not set. Cannot authenticate to npm registry."
    exit 1
else
    export CI_NPM_REGISTRY_URI=`npm config get registry`
    export CI_NPM_REGISTRY=`node -p "const url=require('url').parse(process.env.CI_NPM_REGISTRY_URI);url.host+url.path;"`
    echo 'using npm registry ' ${CI_NPM_REGISTRY}' with value of environment variable NPM_REGISTRY_TOKEN to authenticate'
    echo //${CI_NPM_REGISTRY}:_authToken=${NPM_REGISTRY_TOKEN} > ~/.npmrc
fi
