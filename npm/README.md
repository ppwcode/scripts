# npm

- [`authenticateWithRegistry.sh`](authenticateWithRegistry.md): create an `~/.npmrc file` with credentials to access the
  private npm registry defined in the project-specific `.npmrc`
- [`authenticateWithAzureArtifactsRegistry.sh`](authenticateWithAzureArtifactsRegistry.md): create an `~/.npmrc file`
  with credentials to access a private npm registry defined in the project-specific `.npmrc` on Azure DevOps
- [`testOnNode10-8-6.sh`](testOnNode10-8-6.md): for all major Node LTS versions, do a clean `npm install` and `npm test`
  in that version, and end with a clean `npm install` in the most recent major LTS version
- [`testCi.sh`](testCi.md): test an `npm` project on a Continuous Integration platform
