#!/usr/bin/env bash

#    Copyright 2022-2022 PeopleWare n.v.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Create an `~/.npmrc` file, to get access to the npm registry defined in a project specific `.npmrc`.
#
# To work, the environment variable `NPM_REGISTRY_TOKEN` must be set up, and the registry must be specified in the
# project specific `.npmrc`.
#
# Note that this script is written specifically for Azure Artifacts NPM feeds.  The user must generate a Personal Access
# Token that has the package read & write permissions.  That is the token that should be stored in the
# `NPM_REGISTRY_TOKEN` environment variable.

if [ -f ~/.npmrc ]; then
    echo "There already is an ~/.npmrc file. Not changing npm registry authentication."
elif [ -z $NPM_REGISTRY_TOKEN ]; then
    >&2 echo "Environment variable NPM_REGISTRY_TOKEN not set. Cannot authenticate to npm registry."
    exit 1
else
    export CI_NPM_REGISTRY_URI=`npm config get registry`
    export CI_NPM_REGISTRY=`node -p "const url=require('url').parse(process.env.CI_NPM_REGISTRY_URI);url.host+url.path;"`
    export NPM_REGISTRY_TOKEN_BASE64=`node -p "Buffer.from(\"$NPM_REGISTRY_TOKEN\").toString('base64')"`
    echo 'using npm registry ' ${CI_NPM_REGISTRY}' with value of environment variable NPM_REGISTRY_TOKEN to authenticate'
    cat << EOF > ~/.npmrc
//${CI_NPM_REGISTRY}:username=does_not_matter
//${CI_NPM_REGISTRY}:email=does_not_matter
//${CI_NPM_REGISTRY}:_password=${NPM_REGISTRY_TOKEN_BASE64}
EOF
fi
