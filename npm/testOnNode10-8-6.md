# Test on Node 10, 8, and 6

For all major Node LTS versions, do a clean `npm install` and `npm test` in that version, and end with a clean
`npm install` in the most recent major LTS version. Any failure stops the script.

Must be run from the root of the project.

## Prerequisites

This script requires

- `nvm` (in `~/.nvm/nvm.sh`)
- `node 10` installed with `nvm`
- `node 8` installed with `nvm`
- `node 6` installed with `nvm`
- `npm` (installed with each `node`)
- a `scripts.test` defined in the `package.json` of the project

## Intended use

This script is useful for developers, on a local machine, who wish to test their code in all major Node LTS versions,
and get a coffee.

Normally, you develop in the latest major Node LTS version, and test continuously. Yet, you want your code to be
backward compatible with older major Node LTS versions. So, now and again, you test your code against the older
versions.

For these tests, you have to do a fresh `npm install`, because the versions of the modules you get depend not only on
your dependencies in `package.json` and `package-lock.json`, but also on the Node version you use.

So, this script does that switching for you, for one major Node LTS version after another.

## Limitations

This script can only by used on U\*ixy machines, not on Windows

This script does not install Node versions. It merely uses `nvm` to switch Node versions. The Node versions required
have to be installed on your machine already.

This also means that the minor and patch version used for each major Node LTS version or the latest that happen to be
installed on your machine.

## Improvements

This is a first quick fix.

- Since this is a developer tool, it should also be useable on Windows
- It would be better if we could determine what the Node LTS versions are at runtime. Or, maybe not. Maybe we want to
  parametrize the Node versions, so users can fix them.
- It would be better if we did an `nvm install` when a version is not found.
- It would be better if we checked what the latest minor and patch version of a major Node LTS version is, and made sure
  we install that, if it is not yet installed.
- It would be better of course if the different versions could be tested in parallel. This might be possible by running
  subscripts in different shells, so that they each can have their own Node version under `nvm`. However, we then still
  need a solution for these parallel executions not to share the same `node_modules` folder.
