# Authenticate With Azure Artifacts Registry

Create an `~/.npmrc` file with credentials to access the `npm` `registry` defined in the project specific `.npmrc`, from
a secret in the environment variable `NPM_REGISTRY_TOKEN`. With this setup, `npm` commands will be able to authenticate
to the `registry`.

## Prerequisites

This script requires

- `node`
- `npm` (installed with `node`),
- an `.npmrc` file at the root of the project, and
- a personal access token for the projects Azure Artifacts `npm` registry in the environment variable
  `$NPM_REGISTRY_TOKEN`.

## The need for authentication

An `npm` project depends on artifacts available from a remote registry. Some `npm` projects publish an artifact to a
remote registry.

If the `npm` project depends only on publicly available artifacts, and does not publish an artifact itself, no further
configuration is required.

However, when an `npm` project artifact is published to a registry, we need to authenticate to that registry with an
account that has write permissions. Public `npm` projects will be published in the [public `npm` registry]. Private
`npm` projects will be published in a private `npm`registry, such as [Azure Artifacts]. With Azure Artifacts, there is
a difference in authentication.  On Windows, a custom tool is provided for the authentication, but on other platforms,
the user must work with a personal access token.

Private `npm` projects might themselves depend on private artifacts, published in a private `npm` registry. Furthermore,
private `npm` projects often prefer to get their dependencies from a private `npm` registry, even if the artifact in
question is public, and available on the [public `npm` registry]. A private `npm` registry, such as [Azure Artifacts],
can be set up as _proxy_ for other repositories, and often offers central control over which artifacts and which
versions are available to the dependent projects. Some artifacts or versions of artifacts might be blocked. All
artifacts used are retained. The latter offers protection against a publicly available version of an artifact that your
private projects depend on becoming unavailable. [This has been known to happen]. In this case, we need to authenticate
to the private `npm` registry with an account that has read or read-write permissions, even before we can do
`npm install`.

## .npmrc

An `npm` project has a need to authenticate with a registry, and uses this script, _must_ have an `.npmrc` file at its
root. See [npm config] for details.

The `.npmrc` file at the root of the project should at least have the following entries:

    scope = @<ORGANIZATION\*NAME>
    registry = <REGISTRY_URI>
    always-auth = true
    auth-type = legacy
    strict-ssl = true
    ca = null

Any `npm` project that follows our vernacular should be _scoped_, meaning that the name of the project in
`package.json` should be of the form

    {
      "name": "@<ORGANIZATION_NAME>/<ARTIFACT_NAME>",
      …
    }

The `registry = <REGISTRY_URI>` entry says that its value should be used as URI for all `npm` commands (such as
`npm install`, `npm cit`, `npm audit`, `npm publish`, …). If [Azure Artifacts] is used, the `<REGISTRY_URI>` will be of
the form `//pkgs.dev.azure.com/<ORGANIZATION>/<PROJECT>/_packaging/<PRIVATE_FEED>/npm/registry/` with the following
fields:
- `<ORGANIZATION>`: the Azure Devops organization that the feed is located in
- `<PROJECT>`:
  - the name of the Azure Devops project that the feed is located in
  - missing for organization-scoped feeds (in contrast with project-scoped feeds)
- `<PRIVATE_FEED>`: the name of the private feed


## How to authenticate as developer

The authentication for Azure Artifacts NPM feeds does not use the standard `npm login` mechanism.

On Windows, a specific tool is provided `vsts-npm-auth` through the standard NPM feed.  However, since this tool is not
cross-platform, we recommend using an alternative authentication mechanism based on _personal access tokens_.

In [Azure Artifacts], users can create a personal access token and use that as an app specific password.  Such an access
token can be generated in [Azure Artifacts] using the 'User Settings' menu, option 'Personal Access Tokens'.  The access
token should only have the minimum access rights, which are 'Packaging - read & write'.

The personal access token must then be registered in the user-specific `~/.npmrc` as the password for the authenticated
feed.  Do note that the access token can not be added as-is, but must first be BASE64 encoded.

The following lines can then be added to `~/.npmrc`:

```
; begin auth token
//pkgs.dev.azure.com/<ORGANIZATION>/<PROJECT>/_packaging/<PRIVATE_FEED>/npm/registry/:username=does_not_matter
//pkgs.dev.azure.com/<ORGANIZATION>/<PROJECT>/_packaging/<PRIVATE_FEED>/npm/registry/:_password=$NPM_REGISTRY_TOKEN
//pkgs.dev.azure.com/<ORGANIZATION>/<PROJECT>/_packaging/<PRIVATE_FEED>/npm/registry/:email=does_not_matter
//pkgs.dev.azure.com/<ORGANIZATION>/<PROJECT>/_packaging/<PRIVATE_FEED>/npm/:username=does_not_matter
//pkgs.dev.azure.com/<ORGANIZATION>/<PROJECT>/_packaging/<PRIVATE_FEED>/npm/:_password=$NPM_REGISTRY_TOKEN
//pkgs.dev.azure.com/<ORGANIZATION>/<PROJECT>/_packaging/<PRIVATE_FEED>/npm/:email=does_not_matter
; end auth token
```

Where `NPM_REGISTRY_TOKEN` is the BASE64-encoded personal access token.

The following can be used to BASE64-encode the token:

```shell
node -p "Buffer.from(\"$PERSONAL_ACCESS_TOKEN\").toString('base64')"
```


## How to authenticate as automated process (e.g., CI)

The script [`authenticateWithAzureArtifactsRegistry.sh`](authenticateWithAzureArtifactsRegistry.sh) can be used to store
authentication credentials in a user-specific `~/.npmrc` file.  It does the same thing as the manual process above to
store a BASE64-encoded version of a personal access token in the `~/.npmrc` file.  The script assumes that the
environment variable `$NPM_REGISTRY_TOKEN` contains the original personal access token.

The script is safe in the sense that it will not overwrite an existing `~/.npmrc` file.  If the file already exists, the
script will silently do nothing.

As a side effect, the `<REGISTRY_URI>` will also be available in the environment variable `$CI_NPM_REGISTRY` once the
script has run.

It is ok to use a _personal access token_ from a particular team member for the script, assuming that it has minimal
rights.  The personal access tokens generated by [Azure Artifacts] expire over time. The lifetime of a token can be
specified upon creation, but the maximum lifetime currently possible is 1 year.  So there is a forced rotation of these
tokens.  If the developer that provided the token, leaves the team, a new access token should be generated and used in
the build.

In Bitbucket, one _secret_ environment variable `$NPM_REGISTRY_TOKEN` can be set up at the team level for all projects
in the team.

It is good practice to rotate this token regularly.  As mentioned, this can be enforced by further limiting the lifetime
of a token.

## References

This script is based on the documentation on [Azure Artifacts], shown when activating `connect to feed`.


[public `npm` registry]: https://www.npmjs.com/search?q=ppwcode
[myget]: https://www.myget.org
[this has been known to happen]: https://www.theregister.co.uk/2016/03/23/npm_left_pad_chaos/
[npm config]: https://docs.npmjs.com/misc/config
[Azure Artifacts]: https://docs.microsoft.com/nl-nl/azure/devops/artifacts/feeds/feed-permissions?view=azure-devops
