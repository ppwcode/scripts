#!/usr/bin/env bash

#    Copyright 2018-2018 PeopleWare n.v.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# For all major Node LTS versions, do a clean npm install and npm test in that version, and end with a clean
# npm install in the most recent major LTS version. Any failure stops the script.
# Run from the root of the project.

set -e

. ~/.nvm/nvm.sh

rm -Rf ./node_modules
nvm use 10
npm install
npm test

rm -Rf ./node_modules
nvm use 8
npm install
npm test

rm -Rf ./node_modules
nvm use 6
npm install
npm test

rm -Rf ./node_modules
nvm use 10
npm install

set +e
