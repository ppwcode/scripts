
**_NOTE:_ This repository was hosted at [https://bitbucket.org/ppwcode/scripts](https://bitbucket.org/ppwcode/scripts)
up until version `1.12.2`. Because of a change in how BitBucket charges, the repository was forked at
[https://bitbucket.org/peopleware/scripts](https://bitbucket.org/peopleware/scripts), and future evolution will happen
there.**

**This has the effect that any existing repository that has
[https://bitbucket.org/ppwcode/scripts](https://bitbucket.org/ppwcode/scripts) as submodule
will continue to work with the selected tag (this would not have been possible if the repository location changed).
_All such repositories should switch to
[https://bitbucket.org/peopleware/scripts](https://bitbucket.org/peopleware/scripts) at their convenience to get future
updates._**

**In 2024-Q3, all files will be removed from this (original) repository. At that time, repositories that have the `HEAD`
of this repository as submodule (which is not the intended way this should be used) will fail. Such repositories must
switch to [https://bitbucket.org/peopleware/scripts](https://bitbucket.org/peopleware/scripts)’s latest tag at that
time.**

# Scripts

Collection of scripts, for use in different kinds of git projects.

These scripts are geared foremost for use in a CI. Yet, some scripts are added for use as a developer on the local
machine.

Some scripts are targeted at special environments, e.g., a specific CI, or a specific language. They are kept together
in one collection nevertheless. The overhead of having scripts in a project that are not applicable is negligible
relative to the overhead of splitting out this collection.

## Usage

Include this repository as a _submodule_ in your git repository at `scripts/common`, pointing to a tagged version of
this repository. You should add local scripts next to this submodule, in `scripts/`.

The inclusion should be done with an anonymous HTTP git URL as origin.

## Contents

- [npm](npm/README.md)
  - [`authenticateWithRegistry.sh`](npm/authenticateWithRegistry.md): create an `~/.npmrc file` with credentials to
    access the npm registry defined in the project-specific `.npmrc`
  - [`testOnNode10-8-6.sh`](npm/testOnNode10-8-6.md): for all major Node LTS versions, do a clean `npm install` and
    `npm test` in that version, and end with a clean `npm install` in the most recent major LTS version
  - [`testCi.sh`](npm/testCi.md): test an `npm` project on a Continuous Integration platform
- [git](git/README.md)
  - [`tagTravisGithub.sh`](git/tagTravisGithub.md): tag a successful Travis run on Github
  - [`tagBitbucket.sh`](git/tagBitbucket.md): tag a successful Pipelines run on Bitbucket
  - [`pushToRemote.md`](git/pushToRemote.md): push the current head to a target remote
- [terraform](terraform/README.md)
  - [`getTerraformOutput.sh`](terraform/getTerraformOutput.md): get the output values for the terraform configuration and store in environment variable
- [azure](azure/README.md)
  - [`azLoginWithServicePrincipal.sh`](azure/azLoginWithServicePrincipal.md): login to Azure as the Service Principal whose credentials have been provided
