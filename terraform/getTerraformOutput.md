# Get Terraform output from Azure

Change into the `./terraform` directory, initialize terraform with the shared state stored in Microsoft Azure
and store the output in the `TERRAFORM_OUTPUT` environment variable.

In order for this to work correctly, the script **must be called with a "dot space script" command**, otherwise
the script will be called under another shell and be unable to update the environment variables for the current shell.

i.e. it should be called like this:
```bash
. ./getTerraformOutput.sh
```
**NOT**
```
./getTerraformOutput.sh
```

## Prerequisites

This script requires

- `terraform`
- a subdirectory called `terraform` where all relevant files reside
- environment variables for authentication to Azure to be set. Specifically, the script requires:
  - `$ARM_TENANT_ID`
  - `$ARM_CLIENT_ID`
  - `$ARM_CLIENT_SECRET`

