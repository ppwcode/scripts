# Generate NGSW config

## Requires

`@angular/pwa` to be installed.

## Usage

    > node scripts/hosting/dynbase/ngsw-config.js <prefix_path>

## What it does

The script generates an `ngsw.json` file and used the prefix as base href.
