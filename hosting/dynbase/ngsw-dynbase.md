# Generate NGSW config

## Requires

`@angular/pwa` to be installed.

## Usage

    > node scripts/hosting/dynbase/ngsw-dynbase.js <prefix_path> <index_name:optional>

### Example

    > node scripts/hosting/dynbase/ngsw-dynbase.js /ui/immutable/00000 home

If `index_name` is not provided, it will default to `index`. The html extension is added automatically so don't include
this.

## What it does

The script changes `ngsw.json` to make the url of the index html dynamic.
