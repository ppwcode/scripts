'use strict'
/**
 * The problem: ngsw.json isn't prefixed correctly and there is no angular way to configure these files.
 * Solution: This script will override the assetGroup content of ngsw.json with our prefix path + 'filename'.
 * We use this script every time we build our angular project.
 */
const path = require('path')
const fs = require('fs')
const ngswPath = path.join(__dirname, '..', '..', '..', '..', 'dist', 'ngsw.json')
const ngsw = require(ngswPath)

const args = process.argv.slice(2)

const applyDynamicBase = () => {
  const indexName = (args.length >= 2 && args[1]) || 'index'
  const indexHtml = `/${indexName}.html`

  if (ngsw.index === indexHtml) {
    ngsw.index = '.' + indexHtml
  }
  for (const assetGroup of ngsw.assetGroups) {
    const indexHtmlIndex = assetGroup.urls.findIndex(url => url.includes(indexHtml))
    if (indexHtmlIndex > -1) {
      assetGroup.urls[indexHtmlIndex] = '.' + indexHtml
    }
    const patternIndex = assetGroup.patterns.findIndex(pattern => pattern.includes(`${indexName}\\.html`))
    assetGroup.patterns[patternIndex] = `\\/(?:.+\\/)?${indexName}\\.html`
  }

  fs.writeFile(ngswPath, JSON.stringify(ngsw, null, 4), err => {
    if (err) {
      console.error('File could not be written!', err)
      throw err
    }
  })
}

const buildNumber = process.env.BITBUCKET_BUILD_NUMBER

if (args.length === 0) {
  console.error('please specify a prefix path')
} else {
  const dynBaseBase = args[0]
  const dynBase = buildNumber !== undefined ? dynBaseBase.replace('00000', buildNumber.padStart(5, '0')) : dynBaseBase
  applyDynamicBase(dynBase)
}
