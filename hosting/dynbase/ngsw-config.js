const { exec } = require('child_process')

const args = process.argv.slice(2)

if (args.length === 0) {
  console.error('please specify a prefix path')
} else {
  generateNgswConfig(args[0])
}

function generateNgswConfig (prefixPath) {
  let buildNumber = addLeadingZeros(process.env.BITBUCKET_BUILD_NUMBER)
  exec(`ngsw-config dist ngsw-config.json ${prefixPath}/${buildNumber}`)
}

function addLeadingZeros (number) {
  if (number === undefined) {
    number = '00000'
  }

  while (number.length < 5) {
    number = '0' + number
  }
  return number
}
